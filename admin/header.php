<?php include("cek.php"); error_reporting(0); ?>

<?php include ('cek_level.php'); ?>

<html>
<?php include ('head.php'); ?>

    <body class="skin-black">
        
        <header class="header">
            <a class="logo">
                Administrator
            </a>
            
            <nav class="navbar navbar-static-top" role="navigation">
				<div><marquee><h4>Inventaris Sarana dan Prasarana di SMKN 1 CIOMAS </h4></marquee></div>
            </nav>
        </header>
		
        <div class="wrapper row-offcanvas row-offcanvas-left">
            
            <aside class="left-side sidebar-offcanvas">
                
                <section class="sidebar">
                    
                    <ul class="sidebar-menu">
                        <li>
                            <a href="dashboard.php">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
						<li>
                            <a href="inventaris.php">
                                <i class="fa fa-tags"></i> <span>Inventarisir</span>
                            </a>
                        </li>
						<li>
                            <a href="proses.php">
                                <i class="fa fa-gears"></i> <span>Proses</span>
                            </a>
                        </li>
						<li class="treeview">
                            <a>
                                <i class="fa fa-folder"></i>
                                <span>Referensi</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="jenis.php"><i class="fa fa-angle-double-right"></i> Jenis </a></li>
                                <li><a href="ruang.php"><i class="fa fa-angle-double-right"></i> Ruang </a></li>
								<li><a href="pegawai.php"><i class="fa fa-angle-double-right"></i> Pegawai </a></li>
								<li><a href="petugas.php"><i class="fa fa-angle-double-right"></i> Petugas </a></li>
								<li><a href="level.php"><i class="fa fa-angle-double-right"></i> Level </a></li>
                            </ul>
                        </li>
						<li>
                            <a href="laporan.php">
                                <i class="fa fa-book"></i> <span>Laporan</span>
                            </a>
                        </li>
                    </ul>
					<div class="sidebar-footer hidden-small">
                        <a href="arsip.php" data-toggle="tooltip" data-placement="top" title="Arsip Data">
                            <span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>
                        </a>
						<a href="profil.php" data-toggle="tooltip" data-placement="top" title="Profil">
                            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                        </a>
						<a href="logout.php" data-toggle="tooltip" data-placement="top" title="Logout" 
						onClick = "return confirm('Apakah Anda yakin akan keluar?')">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                </section>
                
            </aside>

            <aside class="right-side">

                <section class="content">

					<div class="row">
                        
                        <section class="col-lg-12">