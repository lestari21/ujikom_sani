<?php include("cek.php"); error_reporting(0); ?>

<?php include ('cek_level.php'); ?>

<?php include ('header.php'); ?>
                            <div class="box box-primary">
                                <div class="box-header">

                                    <i class="fa fa-edit"></i>
									<h3 class="box-title">
                                        Input Level
                                    </h3>
                                </div>
                                <div class="box-body table-responsive">
									
                                    <form role="form" method="post" action="simpan_level.php?">
                                    <div class="box-body">
										
										<div class="form-group">
											<label>Nama Level</label>
                                        <div class="input-group col-md-4">
                                            <div class="input-group-addon">
                                                <i class="fa fa-gears"></i>
                                            </div>
                                            <input name="nama_level" pattern="[A-Za-z ]+" title="Hanya Boleh Huruf" type="text" class="form-control" placeholder="Masukkan Nama Level"  maxlength="15" required autofocus />
                                        </div>
										</div>
										
                                    </div>

                                    <div class="box-body">
                                        <button type="submit" class="btn btn-outline btn-primary">Submit</button>
										<button type="reset" class="btn btn-outline btn-danger">Reset</button>
                                    </div>
                                </form>
									
                                </div>
                            </div>
							
                            <div class="box box-primary">
                                <div class="box-header">
								
									<div class="pull-right box-tools"> 
										
										<div class="btn-group">
                                           <button class="btn btn-primary fa fa-print" data-toggle="dropdown"></button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li><a href="print_level.php">Print to PDF</a></li>
                                                <li><a href="proses_level.php">Export to Excel</a></li>
                                            </ul>
                                        </div> &nbsp;

									</div>
								
                                    <i class="fa fa-file-text"></i>
									<h3 class="box-title">
                                        Data Level
                                    </h3>
                                </div>
                                <div class="box-body table-responsive">
									
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
											<th> No </th>
											<th> Nama Level</th>
											<th> Aksi </th>
										</thead>
										<tbody>
                                            <?php
												include "koneksi.php";
												$no=1;
												$select=mysqli_query($conn,"SELECT * FROM level where status_hapus='1' order by nama_level ASC");
												while($data=mysqli_fetch_array($select))
												{
											?>
                        
											<tr class="success">
												<td><?php echo $no++; ?></td>
												<td><?php echo $data['nama_level'] ?></td>
												<td><a href="#"><button type="button" class="btn btn-outline btn-primary fa fa-edit" data-toggle="modal" data-target="#myModal<?php echo $data['id_level']; ?>" title="Edit Data"></button></a>
													<a href="hapus_refresh_level.php?id_level=<?php echo $data['id_level']; ?>"><button type="button" class="btn btn-outline btn-danger fa fa-trash-o" data-toggle="tooltip" title="Hapus Data"
													onClick = "return confirm('Apakah Anda yakin akan menghapus data ini?')"></button></a>
												</td>
       										</tr>
											
											<div class="modal fade" id="myModal<?php echo $data['id_level']; ?>" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Data Level</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" action="update_level.php" method="POST">

                        <?php
                        $id_level = $data['id_level']; 
                        $query_edit = mysqli_query($conn,"SELECT * FROM level WHERE id_level='$id_level'");
                        //$result = mysqli_query($conn, $query);
                        while ($row = mysqli_fetch_array($query_edit)) {  
                        ?>

                        <input type="hidden" name="id_level" value="<?php echo $row['id_level']; ?>">

                        <div class="form-group">
                          <label>Nama Level</label>
                          <input type="text" pattern="[A-Za-z ]+" title="Hanya Boleh Huruf" name="nama_level" class="form-control" value="<?php echo $row['nama_level']; ?>" required />      
                        </div>
                        
                        <div class="modal-footer">  
                          <button type="submit" class="btn btn-success">Update</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>

                        <?php 
                        }
                        //mysql_close($host);
                        ?>        
                      </form>
                  </div>
                </div>
                
              </div>
            </div>
											
											
											<?php } ?>
                                        </tbody>
                                    </table>
									
                                </div>
                            </div>
<?php include ('footer.php'); ?>