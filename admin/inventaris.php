<?php include("cek.php"); error_reporting(0); ?>

<?php include ('cek_level.php'); ?>

<?php
 
include "koneksi.php";
$query = "SELECT max(kode_inventaris) as maxKode FROM inventaris";
$hasil = mysqli_query($conn,$query);
$data = mysqli_fetch_array($hasil);
$kodeInventaris = $data['maxKode'];
$noUrut = (int) substr($kodeInventaris, 4, 4);
$noUrut++;
$char = "IVTR";
$kodeInventaris = $char . sprintf("%04s", $noUrut);
?>

<?php include ('header.php'); ?>
                            
							<div class="box box-primary">
                                <div class="box-header">
								
                                    <i class="fa fa-edit"></i>
									<h3 class="box-title">
                                        Input Data Inventarisir
                                    </h3>
                                </div>
                                <div class="box-body table-responsive">
									
                                    <form role="form" method="post" action="simpan_inventaris.php">
                                    <div class="box-body">
										
										<div class="form-group col-md-4">
											<label>Kode Inventaris</label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-barcode"></i>
                                            </div>
                                            <input name="kode_inventaris" value="<?php echo $kodeInventaris; ?>" type="text" class="form-control" maxlength="10" required readonly />
                                        </div>
										</div>
										
										<div class="form-group col-md-4">
											<label>Keterangan</label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-bars"></i>
                                            </div>
                                            <input name="keterangan_barang" type="text" pattern="[A-Za-z ]+" title="Hanya Boleh Huruf" class="form-control" placeholder="Masukkan Keterangan" maxlength="25" required autofocus />
                                        </div>
										</div>
										
										<div class="form-group col-md-4">
											<label>Jenis Barang </label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-qrcode"></i>
                                            </div>
                                            <select name="id_jenis" required class="select2_group form-control">
												<option value="">--- Pilih Jenis Barang ---</option>
												<?php
												include "koneksi.php";
												$select=mysqli_query($conn,"select * from jenis where status_hapus='1'");
												while($data=mysqli_fetch_array($select)){
												?>
                                                    <option value="<?php echo $data['id_jenis'];?>"><?php echo $data['kode_jenis'];?> - <?php echo $data['nama_jenis'];?></option>
												<?php }?>
                                            </select>
                                        </div>
										</div>
										
										<div class="form-group col-md-4">
											<label>Nama Barang</label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-inbox"></i>
                                            </div>
                                            <input name="nama_barang" type="text" pattern="[A-Za-z ]+" title="Hanya Boleh Huruf" class="form-control" placeholder="Masukkan Nama Barang"  maxlength="30" required autofocus />
                                        </div>
										</div>
										
										<div class="form-group col-md-4">
											<label>Jumlah Barang</label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-shopping-cart"></i>
                                            </div>
                                            <input name="jumlah" type="number" pattern="[0-9 ]+" title="Hanya Boleh Angka" class="form-control" placeholder="Masukkan Jumlah Barang"  maxlength="10" required autofocus />
                                        </div>
										</div>
										
										<div class="form-group col-md-4">
											<label>Ruang Penyimpanan </label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-building-o"></i>
                                            </div>
                                            <select name="id_ruang" required class="select2_group form-control">
												<option value="">--- Pilih Ruang Penyimpanan ---</option>
												<?php
												include "koneksi.php";
												$select=mysqli_query($conn,"select * from ruang where status_hapus='1'");
												while($data=mysqli_fetch_array($select)){
												?>
                                                    <option value="<?php echo $data['id_ruang'];?>"><?php echo $data['kode_ruang'];?> - <?php echo $data['nama_ruang'];?></option>
												<?php }?>
                                            </select>
                                        </div>
										</div>
										
										<div class="form-group col-md-4">
											<label>Kondisi Barang</label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-retweet"></i>
                                            </div>
                                            <input name="kondisi" type="text" pattern="[A-Za-z ]+" title="Hanya Boleh Huruf" class="form-control" placeholder="Masukkan Kondisi Barang" maxlength="25" required autofocus />
                                        </div>
										</div>
										
										<div class="form-group col-md-4">
											<label>Petugas </label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <select name="id_petugas" required class="select2_group form-control">
												<option value="">--- Pilih Nama Petugas ---</option>
												<?php
												include "koneksi.php";
												$select=mysqli_query($conn,"select * from petugas where status_hapus_p='1'");
												while($data=mysqli_fetch_array($select)){
												?>
                                                    <option value="<?php echo $data['id_petugas'];?>"><?php echo $data['nama_petugas'];?></option>
												<?php }?>
                                            </select>
                                        </div>
										</div>
										
										<div class="form-group col-md-4">
                                        <div class="input-group col-md-11">
                                            <div class="input">
                                            </div>
										</div>
										</div> </br>&nbsp;</br>&nbsp;
                                        
                                    </div>
									
                                    <div class="box-body ">
                                        <button type="submit" class="btn btn-outline btn-primary">Submit</button>
										<button type="reset" class="btn btn-outline btn-danger">Reset</button>
                                    </div>
                                </form>
									
                                </div>
                            </div>
							
                            <div class="box box-primary">
                                <div class="box-header">

									<div class="pull-right box-tools">
										<div class="btn-group">
                                           <button class="btn btn-primary fa fa-print" data-toggle="dropdown"></button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li><a href="print_inventaris.php">Print to PDF</a></li>
                                                <li><a href="proses_inventaris.php">Export to Excel</a></li>
                                            </ul>
                                        </div> &nbsp;
									</div>
								
                                    <i class="fa fa-file-text"></i>
									<h3 class="box-title">
                                        Data Inventarisir
                                    </h3>
                                </div>
                                <div class="box-body table-responsive">
									
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
											<th> No </th>
											<th> Kode Inventaris</th>
											<th> Nama Barang </th>
											<th> Kondisi </th>
											<th> Keterangan </th>
											<th> Jumlah </th>
											<th> Tanggal Register </th>
											<th> Aksi </th>
										</thead>
										<tbody>
                                            <?php
												include "koneksi.php";
												$no=1;
												$select=mysqli_query($conn,"SELECT * FROM inventaris where status_hapus_i='1'");
												while($data=mysqli_fetch_array($select))
												{
											?>
                        
											<tr class="success">
												<td><?php echo $no++; ?></td>
												<td><?php echo $data['kode_inventaris'] ?></td>
												<td><?php echo $data['nama_barang'] ?></td>
												<td><?php echo $data['kondisi'] ?></td>
												<td><?php echo $data['keterangan_barang'] ?></td>
												<td><?php echo $data['jumlah'] ?></td>
												<td><?php echo $data['tanggal_register'] ?></td>
												<td><a href="#"><button type="button" class="btn btn-outline btn-primary fa fa-edit" data-toggle="modal" data-target="#myModal1<?php echo $data['id_inventaris']; ?>" title="Edit Data"></button></a>
													<a href="hapus_refresh_inventaris.php?id_inventaris=<?php echo $data['id_inventaris']; ?>"><button type="button" class="btn btn-outline btn-danger fa fa-trash-o" data-toggle="tooltip" title="Hapus Data"
													onClick = "return confirm('Apakah Anda yakin akan menghapus data ini?')"></button></a>
													<a href="#"><button type="button" class="btn btn-outline btn-success fa fa-eye" data-toggle="modal" data-target="#myModal<?php echo $data['id_inventaris']; ?>" title="Detail Data"></button></a>
												</td>
       										</tr>
											
											<div class="modal fade" id="myModal1<?php echo $data['id_inventaris']; ?>" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Data Inventaris</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" action="update_inventaris.php" method="POST">

                        <?php
                        $id_inventaris = $data['id_inventaris']; 
                        $query_edit = mysqli_query($conn,"SELECT * FROM inventaris i left join jenis j on j.id_jenis=i.id_jenis
																					left join ruang r on r.id_ruang=i.id_ruang
																					left join petugas p on p.id_petugas=i.id_petugas
																				WHERE id_inventaris='$id_inventaris'");
                        //$result = mysqli_query($conn, $query);
						$select_jenis=mysqli_query($conn,"SELECT * FROM `jenis` where status_hapus='1'");
						$select_ruang=mysqli_query($conn,"SELECT * FROM `ruang` where status_hapus='1'");
						$select_petugas=mysqli_query($conn,"SELECT * FROM `petugas` where status_hapus_p='1'");
                        while ($row = mysqli_fetch_array($query_edit)) {  
                        ?>
						
                        <input type="hidden" name="id_inventaris" value="<?php echo $row['id_inventaris']; ?>">

                        <div class="form-group">
							<label>Kode Inventaris</label>
                            <input name="kode_inventaris" value="<?php echo $data['kode_inventaris']; ?>" type="text" class="form-control" placeholder="Masukkan Kode Inventaris"  maxlength="10" required readonly />
                        </div>
										
						<div class="form-group">
							<label>Nama Barang</label>
                            <input name="nama_barang" pattern="[A-Za-z ]+" title="Hanya Boleh Huruf" value="<?php echo $data['nama_barang']; ?>" pattern="[A-Za-z]+" title="Hanya Boleh Huruf" type="text" class="form-control" placeholder="Masukkan Nama Barang"  maxlength="30" required />
                        </div>
										
						<div class="form-group">
							<label>Kondisi Barang</label>
                            <input name="kondisi" pattern="[A-Za-z ]+" title="Hanya Boleh Huruf" value="<?php echo $data['kondisi']; ?>" pattern="[A-Za-z]+" title="Hanya Boleh Huruf" type="text" class="form-control" placeholder="Masukkan Kondisi Barang" maxlength="25" required />
                        </div>
										
						<div class="form-group">
							<label>Keterangan</label>
                            <input name="keterangan_barang" pattern="[A-Za-z ]+" title="Hanya Boleh Huruf" value="<?php echo $data['keterangan_barang']; ?>" pattern="[A-Za-z]+" title="Hanya Boleh Huruf" type="text" class="form-control" placeholder="Masukkan Keterangan" maxlength="25" required />
                        </div>
										
						<div class="form-group">
							<label>Jumlah Barang</label>
                            <input name="jumlah" pattern="[0-9 ]+" title="Hanya Boleh Huruf" value="<?php echo $data['jumlah']; ?>" type="number" pattern="[0-9]+" title="Hanya Boleh Angka" class="form-control" placeholder="Masukkan Jumlah Barang"  maxlength="10" required />
                        </div>
										
						<div class="form-group">
							<label>Jenis Barang </label>
                            <select name="id_jenis" required class="select2_group form-control">
								<?php while($row_jenis=mysqli_fetch_array($select_jenis)){ ?>
                                    <option value="<?php echo $row_jenis['id_jenis'];?>"
										<?php if($row['id_jenis']==$row_jenis['id_jenis']) { echo "selected"; } ?>>
										<?php  echo $row_jenis['nama_jenis']; ?>
									</option>
								<?php } ?>
                            </select>
						</div>
										
						<div class="form-group">
							<label>Ruang Penyimpanan </label>
                                <select name="id_ruang" required class="select2_group form-control">
									<?php while($row_ruang=mysqli_fetch_array($select_ruang)){ ?>
                                        <option value="<?php echo $row_ruang['id_ruang'];?>"
											<?php if($row['id_ruang']==$row_ruang['id_ruang']) { echo "selected"; } ?>>
											<?php  echo $row_ruang['nama_ruang']; ?>
										</option>
									<?php } ?>
                                </select>
						</div>
										
						<div class="form-group">
							<label>Petugas </label>
                                <select name="id_petugas" required class="select2_group form-control">
									<?php while($row_petugas=mysqli_fetch_array($select_petugas)){ ?>
                                        <option value="<?php echo $row_petugas['id_petugas'];?>"
											<?php if($row['id_petugas']==$row_petugas['id_petugas']) { echo "selected"; } ?>>
											<?php  echo $row_petugas['nama_petugas']; ?>
										</option>
									<?php } ?>
                                </select>
                         </div>
						
                        <div class="modal-footer">  
                          <button type="submit" class="btn btn-success">Update</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>

                        <?php 
                        }
                        //mysql_close($host);
                        ?>        
                      </form>
                  </div>
                </div>
                
              </div>
            </div>
											
											
											<div class="modal fade" id="myModal<?php echo $data['id_inventaris']; ?>" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Detail Data Inventaris</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" method="POST">
						
                        <?php
                        $id_inventaris = $data['id_inventaris']; 
                        $query_detail = mysqli_query($conn,"SELECT * FROM inventaris i left join jenis j on j.id_jenis=i.id_jenis
																					   left join ruang r on r.id_ruang=i.id_ruang
																					   left join petugas p on p.id_petugas=i.id_petugas
																					   WHERE id_inventaris='$id_inventaris'");
                        //$result = mysqli_query($conn, $query);
                        while ($row = mysqli_fetch_array($query_detail)) {  
                        ?>
									
						<div class="form-group">
							Kode Inventaris &nbsp &nbsp
							: &nbsp <?php echo $row['kode_inventaris']; ?>  
						</div>
						
						<div class="form-group">
							Nama Barang &nbsp &nbsp &nbsp;&nbsp
							: &nbsp <?php echo $row['nama_barang']; ?>  
						</div>
						
						<div class="form-group">
							Kondisi &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp;&nbsp;
							: &nbsp <?php echo $row['kondisi']; ?>  
						</div>
						
						<div class="form-group">
							Keterangan &nbsp &nbsp &nbsp &nbsp &nbsp
							: &nbsp <?php echo $row['keterangan_barang']; ?>  
						</div>
						
						<div class="form-group">
							Jumlah &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
							: &nbsp <?php echo $row['jumlah']; ?>  
						</div>
						
						<div class="form-group">
							Tanggal Register &nbsp 
							: &nbsp <?php echo $row['tanggal_register']; ?>  
						</div>
						
						<div class="form-group">
							Kode Jenis &nbsp &nbsp &nbsp &nbsp &nbsp;&nbsp
							: &nbsp <?php echo $row['kode_jenis']; ?>  
						</div>
						
						<div class="form-group">
							Nama Jenis &nbsp &nbsp &nbsp &nbsp &nbsp  
							: &nbsp <?php echo $row['nama_jenis']; ?>  
						</div>
						
						<div class="form-group">
							Kode Ruang &nbsp &nbsp &nbsp &nbsp 
							: &nbsp <?php echo $row['kode_ruang']; ?>  
						</div>
						
						<div class="form-group">
							Nama Ruang &nbsp &nbsp;&nbsp;&nbsp;&nbsp;
							: &nbsp <?php echo $row['nama_ruang']; ?>  
						</div>
						
						<div class="form-group">
							Nama Petugas &nbsp;&nbsp &nbsp;
							: &nbsp <?php echo $row['nama_petugas']; ?>  
						</div>
									
                        <div class="modal-footer">  
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>

                        <?php 
                        }
                        //mysql_close($host);
                        ?>        
                      </form>
                  </div>
                </div>
                
              </div>
            </div>
											
											
											<?php } ?>
                                        </tbody>
                                    </table>
									
                                </div>
                            </div>
<?php include ('footer.php'); ?>