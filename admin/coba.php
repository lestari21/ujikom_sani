<html>
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1" />
	<title> Inventaris</title>
</head>

<body style="font-family:Arial, Helvetica, sans-serif">

<!------------- Kontainer ------------------------------------------->
<table border="0" width="100%" align="center">
<tr><td>

<!------------- Bagian Header --------------------------------------->
<table border="1" width="100%" align="center" cellspacing="0" cellpadding="">
	<tr align="center"> 
		<td><br><h1> Inventaris Sarana dan Prasarana di SMK </h1><br></td>
	</tr>
</table>

<!------------- Bagian Navigasi --------------------------------------->
<table border="1" width="100%" align="center" cellspacing="0" cellpadding="">
	<tr>
		<td><div align="center"><a href="index.php">Inventaris</a></div></td>
		<td><div align="center"><a href="index.php?page=siswa">Peminjaman </a></div></td>
		<td><div align="center"><a href="index.php?page=kelas">Pengembalian</a> </div></td>
		<td><div align="center"><a href="index.php?page=jurusan">Laporan </a></div></td>
		<div align="center"><a href="index.php?page=inputsiswa"></a></div>
	</tr>
</table>

<!------------- Bagian Isi --------------------------------------->
<table border="1" width="100%" align="center" cellspacing="0" cellpadding="">
	<tr><td>
			<?php
			$page = (isset($_GET['page']))? $_GET['page'] : "main";
			switch ($page) {
			case 'home': include "inventaris.php"; break;
			case 'siswa' : include "peminjaman.php"; break;
			case 'kelas' : include "pengembalian.php"; break;
			case 'laporan': include "laporan.php"; break;
			case 'main' : include "welcome.php"; break;
			}
			?>
	</td></tr>
</table>

<!------------- Bagian Footer --------------------------------------->
<table width="100%" border="1" cellpadding="3" cellspacing="0">
	<tr>
		<td align="center">Copyright @ Inventaris_Sarana_Prasarana_di_smk </td>
	</tr>
</table>

</td></tr>
</table>

</body>
</html>