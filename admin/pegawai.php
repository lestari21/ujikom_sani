<?php include("cek.php"); error_reporting(0); ?>

<?php include ('cek_level.php'); ?>

<?php include ('header.php'); ?>			
							<div class="box box-primary">
                                <div class="box-header">

                                    <i class="fa fa-edit"></i>
									<h3 class="box-title">
                                        Input Data Pegawai
                                    </h3>
                                </div>
                                <div class="box-body table-responsive">
									
                                    <form role="form" method="post" action="simpan_pegawai.php">
                                    <div class="box-body">
										
										<div class="form-group col-md-4">
											<label>Nip </label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-ellipsis-h"></i>
                                            </div>
                                            <input name="nip" pattern="[0-9 ]+" title="Hanya Boleh Angka" type="text" class="form-control" placeholder="Masukkan NIP"  maxlength="16" required autofocus />
                                        </div>
										</div>
										
										<div class="form-group col-md-4">
											<label>Nama Pegawai </label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <input name="nama_pegawai" pattern="[A-Za-z ]+" title="Hanya Boleh Huruf" type="text" class="form-control" placeholder="Masukkan Nama Pegawai"  maxlength="30" required autofocus />
                                        </div>
										</div>
										
										<div class="form-group col-md-4">
											<label>Jenis Kelamin </label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-users"></i>
                                            </div>
											<select name="jk" required class="select2_group form-control">
												<option value="">--- Pilih Jenis Kelamin ---</option>
												<option>Laki-Laki</option>
												<option>Perempuan</option>
                                            </select>       
                                        </div>
										</div>
										
										<div class="form-group col-md-4">
											<label>Alamat</label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-home"></i>
                                            </div>
                                            <input name="alamat" pattern="[0-9A-Za-z-./ ]+" title="Hanya Boleh Huruf & Angka" type="text" class="form-control" placeholder="Masukkan Alamat" maxlength="50" required autofocus />
                                        </div>
										</div>
										
										<div class="form-group col-md-4">
                                        <div class="input-group col-md-11">
                                            <div class="input">
                                            </div>
										</div>
										</div> </br>&nbsp;</br>&nbsp;<br/>&nbsp;
										
                                    </div>

                                    <div class="box-body">
                                        <button type="submit" class="btn btn-outline btn-primary">Submit</button>
										<button type="reset" class="btn btn-outline btn-danger">Reset</button>
                                    </div>
                                </form>
									
                                </div>
                            </div>
                            
                            <div class="box box-primary">
                                <div class="box-header">
								
									<div class="pull-right box-tools">
										<div class="btn-group">
                                           <button class="btn btn-primary fa fa-print" data-toggle="dropdown"></button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li><a href="print_pegawai.php">Print to PDF</a></li>
                                                <li><a href="proses_pegawai.php">Export to Excel</a></li>
                                            </ul>
                                        </div> &nbsp;
									</div>
								
                                    <i class="fa fa-file-text"></i>
									<h3 class="box-title">
                                        Data Pegawai
                                    </h3>
                                </div>
                                <div class="box-body table-responsive">
									
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
											<th> No </th>
											<th> Nip </th>
											<th> Nama Pegawai </th>
											<th> Jenis Kelamin </th>
											<th> Alamat </th>
											<th> Aksi </th>
										</thead>
										<tbody>
                                            <?php
												include "koneksi.php";
												$no=1;
												$select=mysqli_query($conn,"SELECT * FROM pegawai where status='Aktif' AND status_hapus = '1' order by nama_pegawai ASC");
												while($data=mysqli_fetch_array($select))
												{
											?>
                        
											<tr class="success">
												<td><?php echo $no++; ?></td>
												<td><?php echo $data['nip'] ?></td>
												<td><?php echo $data['nama_pegawai'] ?></td>
												<td><?php echo $data['jk'] ?></td>
												<td><?php echo $data['alamat'] ?></td>
												<td><a href="#"><button type="button" class="btn btn-outline btn-primary fa fa-edit" data-toggle="modal" data-target="#myModal<?php echo $data['id_pegawai']; ?>" title="Edit Data"></button></a>
													<a href="hapus_refresh_pegawai.php?id_pegawai=<?php echo $data['id_pegawai']; ?>"><button type="button" class="btn btn-outline btn-danger fa fa-trash-o" data-toggle="tooltip" title="Hapus Data"
													onClick = "return confirm('Apakah Anda yakin akan menghapus data ini?')"></button></a>
													<a href="ubah_status.php?id_pegawai=<?php echo $data['id_pegawai']; ?>"><button type="button" class="btn btn-outline btn-success fa fa-pencil" data-toggle="tooltip" title="Ubah Status"
													onClick = "return confirm('Apakah Anda yakin akan mengubah status data <?php echo $data['nama_pegawai']; ?> menjadi tidak aktif ?')"></button></a>
													
												</td>
       										</tr>
											
											<div class="modal fade" id="myModal<?php echo $data['id_pegawai']; ?>" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Data Pegawai</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" action="update_pegawai.php" method="POST">

                        <?php
                        $id_pegawai = $data['id_pegawai']; 
                        $query_edit = mysqli_query($conn,"SELECT * FROM pegawai WHERE id_pegawai='$id_pegawai'");
                        //$result = mysqli_query($conn, $query);
                        while ($row = mysqli_fetch_array($query_edit)) {  
                        ?>

                        <input type="hidden" name="id_pegawai" value="<?php echo $row['id_pegawai']; ?>">

                        <div class="form-group">
                          <label>Nip</label>
                          <input type="text" name="nip" pattern="[0-9 ]+" title="Hanya Boleh Angka" class="form-control" value="<?php echo $row['nip']; ?>" required />      
                        </div>
						
						<div class="form-group">
                          <label>Nama Pegawai</label>
                          <input type="text" name="nama_pegawai" pattern="[A-Za-z ]+" title="Hanya Boleh Huruf" class="form-control" value="<?php echo $row['nama_pegawai']; ?>" required />      
                        </div>
						
						<div class="form-group">
                          <label>Jenis Kelamin</label>
						  <select name="jk" value="<?php echo $data['jk'];?>" required class="select2_group form-control">
							<option>--- Pilih Jenis Kelamin ---</option>
                            <option value="Laki-Laki" <?php if($data['jk'] == 'Laki-Laki'){ echo 'selected'; } ?>>Laki-Laki</option>
							<option value="Perempuan" <?php if($data['jk'] == 'Perempuan'){ echo 'selected'; } ?>>Perempuan</option>
						  </select>
                        </div>
						
						<div class="form-group">
                          <label>Alamat</label>
                          <input type="text" name="alamat" pattern="[0-9A-Za-z-./ ]+" title="Hanya Boleh Huruf & Angka" class="form-control" value="<?php echo $row['alamat']; ?>" required />      
                        </div>
                        
                        <div class="modal-footer">  
                          <button type="submit" class="btn btn-success">Update</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>

                        <?php 
                        }
                        //mysql_close($host);
                        ?>        
                      </form>
                  </div>
                </div>
                
              </div>
            </div>
											
											<?php } ?>
                                        </tbody>
                                    </table>
									
                                </div>
                            </div>
<?php include ('footer.php'); ?>