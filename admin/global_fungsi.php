<?php
require_once(__DIR__ . '/lib/db.class.php');

function ambil_data_global($tabel, $kolom, $kriteria="", $urutan="", $tambahan="")
	{
		$db    = new DB();
		$query = "SELECT $kolom FROM $tabel ";
		if ($kriteria!="") {
			$query .= " WHERE $kriteria ";
		}
		if ($urutan!="") {
			$query .= " ORDER BY $urutan ";
		}
		if ($tambahan!="") {
			$query .= $tambahan;
		}

		$datas = $db->query($query);
		return $datas;
	}
?>