<?php include("cek.php"); error_reporting(0); ?>

<?php include ('cek_level.php'); ?>
<?php include ('header.php'); ?>  
<a href="arsip.php"><button type="submit" class="btn btn-primary fa fa-mail-reply"> Back </button></a> <br/>&nbsp;
                            <div class="box box-primary">
                                <div class="box-header">
								
									<div class="pull-right box-tools">
										<div class="btn-group">
                                           <button class="btn btn-primary fa fa-print" data-toggle="dropdown"></button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li><a href="print_jenis_terhapus.php">Print to PDF</a></li>
                                                <li><a href="proses_jenis_terhapus.php">Export to Excel</a></li>
                                            </ul>
                                        </div> &nbsp;
									</div>
								
                                    <i class="fa fa-file-text"></i>
									<h3 class="box-title">
                                        Arsip Data Jenis Yang Terhapus
                                    </h3>
                                </div>
								<p>&nbsp; &nbsp; Data Jenis yang sudah dihapus </p>
                                <div class="box-body table-responsive">
									
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
											<th> No </th>
											<th> Kode Jenis </th>
											<th> Nama Jenis </th>
											<th> Keterangan </th>
											<th> Aksi </th>
										</thead>
										<tbody>
                                            <?php
												include "koneksi.php";
												$no=1;
												$select=mysqli_query($conn,"SELECT * FROM jenis where status_hapus='0'");
												while($data=mysqli_fetch_array($select))
												{
											?>
                        
											<tr class="success">
												<td><?php echo $no++; ?></td>
												<td><?php echo $data['kode_jenis'] ?></td>
												<td><?php echo $data['nama_jenis'] ?></td>
												<td><?php echo $data['keterangan_jenis'] ?></td>
												<td><a href="refresh_jenis.php?id_jenis=<?php echo $data['id_jenis']; ?>"><button type="button" class="btn btn-outline btn-success fa fa-refresh" data-toggle="tooltip" title="Refresh Data"
													onClick = "return confirm('Apakah Anda yakin akan merefresh data ini?')"></button></a> 
													<a href="hapus_jenis.php?id_jenis=<?php echo $data['id_jenis']; ?>"><button type="button" class="btn btn-outline btn-danger fa fa-trash-o" data-toggle="tooltip" title="Hapus Data"
													onClick = "return confirm('Apakah Anda yakin akan menghapus data ini? Data akan dihapus secara permanen.')"></button></a>
												</td>
       										</tr>
											<?php } ?>
                                        </tbody>
                                    </table>
									
                                </div>
                            </div>
<?php include ('footer.php'); ?>