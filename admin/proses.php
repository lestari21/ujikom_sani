<?php include 'global_fungsi.php' ?>
<?php
include("cek.php");
error_reporting(0);
?>
<?php
include ('cek_level.php');
?>
<?php include ('header.php');?>
                            <div class="box box-primary">
                                <div class="box-header">
                                    <i class="fa fa-file-text"></i>
									<h3 class="box-title">
                                        Proses Transaksi
                                    </h3>
                                </div>
                                <div class="box-body">
								<div class="container">
								
									<div class="col-md-1"> &nbsp; </div>
								
									<div class="col-md-4">
										<div class="box box-danger">
											<div align="center" class="box-body">
											<img src="../img/2.png" width="90">
											<h4 align="center"> Peminjaman </h4>
											<p align="center"> Input & Data Transaksi Peminjaman Barang inventaris.</p>
											</div>
											<div class="box-footer">
												&nbsp;&nbsp;&nbsp; <a href="input_peminjaman.php"><button type="submit" class="btn btn-primary">Input Peminjaman</button></a> &nbsp;&nbsp;&nbsp;
												<a href="peminjaman.php"><button type="submit" class="btn btn-primary">Data Peminjaman</button></a>
											</div>
										</div>
									</div>
									
									<div class="col-md-1"> &nbsp; </div>
									
									<div class="col-md-4">
										<div class="box box-success">
											<div align="center" class="box-body">
											<img src="../img/1.png" width="90">
											<h4 align="center"> Pengembalian </h4>
											<p align="center"> Input & Data Tansaksi Pengembalian Barang Inventaris. </p>
											</div>
											<div class="box-footer">
												&nbsp; <a href="input_pengembalian.php"><button type="submit" class="btn btn-primary">Input Pengembalian</button></a> &nbsp;&nbsp;
												<a href="pengembalian.php"><button type="submit" class="btn btn-primary">Data Pengembalian</button></a>
											</div> 
										</div>
									</div>
									
								</div>
                                </div>
                            </div>
							
							<div class="col-md-6">
							<div class="box box-primary">
                                <div class="box-body">
								
								<?php 
									$pinjam   = ambil_data_global ("peminjaman", "id_peminjaman", "status_peminjaman = 'Dipinjam'");
									$n_pinjam= count($pinjam);

									$kembali   = ambil_data_global("peminjaman", "id_peminjaman", "status_peminjaman = 'Dikembalikan'");
									$n_kembali = count($kembali);
								?>
								<legend>Chart Transaksi</legend>
								<div class="img-responsive">
									<canvas id="canvas_chart" style="width:100%"></canvas>
									<div>
									<br><br>
										<span style="background-color:#F7464A"> &nbsp;&nbsp;&nbsp;&nbsp; </span> &nbsp; Peminjaman  &nbsp; &nbsp; &nbsp; : <?php echo $n_pinjam; ?>
										<br>
										<span style="background-color:#46BFBD"> &nbsp;&nbsp;&nbsp;&nbsp; </span> &nbsp; Pengembalian &nbsp;&nbsp; : <?php echo $n_kembali; ?>
										<br>
										<span style="background-color:#000000"> &nbsp;&nbsp;&nbsp;&nbsp; </span> &nbsp; Total Transaksi &nbsp; : <?php echo $n_pinjam+$n_kembali; ?>
									<br>
									</div>
								</div>
								<!-- Chart JS -->
								<script type="text/javascript" src="assets/js/vendor/chartjs/Chart.min.js"></script>
								<script type="text/javascript">
									// For a pie chart
									var options = [
									    {
									        value: <?php echo $n_pinjam; ?>,
									        color:"#F7464A",
									        highlight: "#FF5A5E",
									        label: "Peminjaman"
									    },
									    {
									        value: <?php echo $n_kembali; ?>,
									        color: "#46BFBD",
									        highlight: "#5AD3D1",
									        label: "Pengembalian"
									    }
									]
									window.onload = function(){
										var ctx      = document.getElementById("canvas_chart").getContext("2d");
										window.myPie = new Chart(ctx).Pie(options, {
										    animateScale: true

										});
									};
								</script>
                                </div>
                            </div>
							</div>
<?php include ('footer.php'); ?>