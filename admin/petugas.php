<?php include("cek.php"); error_reporting(0); ?>

<?php include ('cek_level.php'); ?>
<?php include ('header.php'); ?>
							<div class="box box-primary">
                                <div class="box-header">

                                    <i class="fa fa-edit"></i>
									<h3 class="box-title">
                                        Input Data Petugas
                                    </h3>
                                </div>
                                <div class="box-body table-responsive">
									
                                    <form role="form" method="post" action="simpan_petugas.php">
                                    <div class="box-body">
										
										<div class="form-group col-md-4">
											<label>Nama Petugas </label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <input name="nama_petugas" pattern="[A-Za-z ]+" title="Hanya Boleh Huruf" type="text" class="form-control" placeholder="Masukkan Nama Petugas"  maxlength="30" required autofocus />
                                        </div>
										</div>
										
										<div class="form-group col-md-4">
											<label>Username </label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <input name="username" pattern="[0-9A-Za-z ]+" title="Hanya Boleh Huruf & Angka" type="text" class="form-control" placeholder="Masukkan Username"  maxlength="20" required autofocus />
                                        </div>
										</div>
										
										<div class="form-group col-md-4">
											<label>Password</label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-lock"></i>
                                            </div>
                                            <input name="password" pattern="[0-9A-Za-z . @ ]+" title="Hanya Boleh Huruf & Angka" type="password" class="form-control" placeholder="Masukkan Password" maxlength="15" required autofocus />
                                        </div>
										</div>
										
										<div class="form-group col-md-4">
											<label>Email</label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </div>
                                            <input name="email" pattern="[0-9A-Za-z._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]+" title="Hanya Boleh Tipe Email ex:admin@gmail.com" type="text" class="form-control" placeholder="Masukkan Email" maxlength="15" required autofocus />
                                        </div>
										</div>
										
										<div class="form-group col-md-4">
											<label> Level </label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-gears"></i>
                                            </div>
                                            <select name="id_level" required class="select2_group form-control">
												<option value="">--- Pilih Level ---</option>
												<?php
												include "koneksi.php";
												$select=mysqli_query($conn,"select * from level where status_hapus='1'");
												while($data=mysqli_fetch_array($select)){
												?>
                                                    <option value="<?php echo $data['id_level'];?>"><?php echo $data['nama_level'];?></option>
												<?php }?>
                                            </select>
                                        </div>
										</div>
										
										<div class="form-group col-md-4">
                                        <div class="input-group col-md-11">
                                            <div class="input">
                                            </div>
										</div>
										</div> </br>&nbsp;</br>&nbsp;<br/>&nbsp;
										
                                    </div>

                                    <div class="box-body">
                                        <button type="submit" class="btn btn-outline btn-primary">Submit</button>
										<button type="reset" class="btn btn-outline btn-danger">Reset</button>
                                    </div>
                                </form>
									
                                </div>
                            </div>
						
                            <div class="box box-primary">
                                <div class="box-header">

									<div class="pull-right box-tools">
										<div class="btn-group">
                                           <button class="btn btn-primary fa fa-print" data-toggle="dropdown"></button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li><a href="print_petugas.php">Print to PDF</a></li>
                                                <li><a href="proses_petugas.php">Export to Excel</a></li>
                                            </ul>
                                        </div> &nbsp;
									</div>
								
                                    <i class="fa fa-file-text"></i>
									<h3 class="box-title">
                                        Data Petugas
                                    </h3>
                                </div>
                                <div class="box-body table-responsive">
									
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
											<th> No </th>
											<th> Nama Petugas </th>
											<th> Email </th>
											<th> Username </th>
											<th> Level </th>
											<th> Aksi </th>
										</thead>
										<tbody>
                                            <?php
												include "koneksi.php";
												$no=1;
												$select=mysqli_query($conn,"SELECT * FROM petugas p left join level l on l.id_level=p.id_level where status_hapus_p = '1' order by nama_petugas ASC");
												while($data=mysqli_fetch_array($select))
												{
											?>
                        
											<tr class="success">
												<td><?php echo $no++; ?></td>
												<td><?php echo $data['nama_petugas'] ?></td>
												<td><?php echo $data['email'] ?></td>
												<td><?php echo $data['username'] ?></td>
												<td><?php echo $data['nama_level'] ?></td>
												<td><a href="#"><button type="button" class="btn btn-outline btn-primary fa fa-edit" data-toggle="modal" data-target="#myModal<?php echo $data['id_petugas']; ?>" title="Edit Data"></button></a>
													<a href="hapus_refresh_petugas.php?id_petugas=<?php echo $data['id_petugas']; ?>"><button type="button" class="btn btn-outline btn-danger fa fa-trash-o" data-toggle="tooltip" title="Hapus Data"
													onClick = "return confirm('Apakah Anda yakin akan menghapus data ini?')"></button></a>
												</td>
       										</tr>
											
											<div class="modal fade" id="myModal<?php echo $data['id_petugas']; ?>" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Data Petugas</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" action="update_petugas.php" method="POST">

                        <?php
                        $id_petugas = $data['id_petugas']; 
                        $query_edit = mysqli_query($conn,"SELECT * FROM petugas p left join level l on l.id_level=p.id_level
																				WHERE id_petugas='$id_petugas'");
                        //$result = mysqli_query($conn, $query);
						$select_level=mysqli_query($conn,"SELECT * FROM `level` where status_hapus='1'");
                        while ($row = mysqli_fetch_array($query_edit)) {  
                        ?>
						
                        <input type="hidden" name="id_petugas" value="<?php echo $row['id_petugas']; ?>">

                        <div class="form-group">
                          <label>Nama Level</label>
                          <input type="text" name="nama_petugas" pattern="[A-Za-z ]+" title="Hanya Boleh Huruf" class="form-control" value="<?php echo $row['nama_petugas']; ?>" required />      
                        </div>
						
						<div class="form-group">
                          <label>Email</label>
                          <input type="text" name="email" pattern="[0-9A-Za-z._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]+" title="Hanya Boleh Tipe Email ex:admin@gmail.com" class="form-control" value="<?php echo $row['email']; ?>" required />      
                        </div>
						
						<div class="form-group">
                          <label>Username</label>
                          <input type="text" name="username" pattern="[0-9A-Za-z ]+" title="Hanya Boleh Huruf & Angka" class="form-control" value="<?php echo $row['username']; ?>" required />      
                        </div>
                        
						<div class="form-group">
                          <label>Password</label>
                          <input type="text" name="password" pattern="[0-9A-Za-z ]+" title="Hanya Boleh Huruf & Angka" class="form-control" value="<?php echo $row['password']; ?>" required />      
                        </div>
						
						<div class="form-group">
							<label> Level </label>
                            <select name="id_level" required class="select2_group form-control">
								<?php while($row_level=mysqli_fetch_array($select_level)){ ?>
                                    <option value="<?php echo $row_level['id_level'];?>"
										<?php if($row['id_level']==$row_level['id_level']) { echo "selected"; } ?>>
										<?php  echo $row_level['nama_level']; ?>
									</option>
								<?php } ?>
                            </select>
                        </div>
						</div>
						
                        <div class="modal-footer">  
                          <button type="submit" class="btn btn-success">Update</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>

                        <?php 
                        }
                        //mysql_close($host);
                        ?>        
                      </form>
                  </div>
                </div>
                
              </div>
            </div>
											<?php } ?>
                                        </tbody>
                                    </table>
									
                                </div>
<?php include ('footer.php'); ?>