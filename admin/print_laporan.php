<?php ob_start(); ?>
<html>
<head>
	<title>Cetak PDF</title>
	<style>
	table {border-collapse:collapse; table-layout:fixed;width: 630px:}
	table td {word-wrap:break-word;width: 15%}
	</style>
</head>
<body>
<h1 style="text-align:center;">Laporan Data Transaksi Inventaris</h1>
<table align="center" border="1" width="100%">
<tr>
	<th align="center">No</th>
	<th align="center">Kode Peminjaman</th>
	<th align="center">Tanggal Pinjam</th>
	<th align="center">Tanggal Kembali</th>
	<th align="center">Nama Pegawai</th>
	<th align="center">Nama Barang</th>
	<th align="center">Jumlah Pinjam</th>
</tr>
		<?php
		include "koneksi.php";
		$no=1;
		$select=mysqli_query($conn,"SELECT * from peminjaman p left join detail_pinjam d on d.kode_peminjaman=p.kode_peminjaman
														 left join inventaris i on d.id_inventaris=i.id_inventaris
														 left join pegawai g on p.id_pegawai=g.id_pegawai
														 where status_peminjaman='Dikembalikan'");
		while($data=mysqli_fetch_array($select))
		{
		?>
		<tr align="center">
			<td width="25" align="center"><?php echo $no++; ?></td>
			<td><?php echo $data['kode_peminjaman']; ?></td>
			<td><?php echo $data['tanggal_pinjam']; ?></td>
			<td><?php echo $data['tanggal_kembali']; ?></td>
			<td><?php echo $data['nama_pegawai']; ?></td>
			<td><?php echo $data['nama_barang']; ?></td>
			<td><?php echo $data['jumlah_pinjam']; ?></td>
		</tr>
		<?php
		}
		?>
</table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();

require_once('../html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('L','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Laporan Transaksi Inventaris.pdf', 'D');
?>