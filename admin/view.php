<?php include("cek.php"); error_reporting(0); ?>

<?php include ('cek_level.php'); ?>
<?php include ('header.php'); ?>
<a href="laporan.php"><button type="submit" class="btn btn-primary fa fa-mail-reply"> Back </button></a> <br/>&nbsp;
                            <div class="box box-primary">
                                <div class="box-header">
                                    <i class="fa fa-file-text"></i>
									<h3 class="box-title">
                                        Data Transaksi Inventaris
                                    </h3>
                                </div>
                                <div class="box-body table-responsive">
									
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
											<th> No </th>
											<th> Kode Peminjaman </th>
											<th> Tanggal Pinjam </th>
											<th> Tanggal Kembali </th>
											<th> Nama Pegawai </th>
											<th> Nama Barang </th>
											<th> Jumlah Pinjam </th>
										</thead>
										<tbody>
                                            <?php
												include "koneksi.php";
												$no=1;
												$select=mysqli_query($conn,"SELECT * from peminjaman p left join detail_pinjam d on d.kode_peminjaman=p.kode_peminjaman
														 left join inventaris i on d.id_inventaris=i.id_inventaris
														 left join pegawai g on p.id_pegawai=g.id_pegawai
														 where status_peminjaman='Dikembalikan'");
												while($data=mysqli_fetch_array($select))
												{
											?>
                        
											<tr class="success">
												<td><?php echo $no++; ?></td>
												<td><?php echo $data['kode_peminjaman'] ?></td>
												<td><?php echo $data['tanggal_pinjam'] ?></td>
												<td><?php echo $data['tanggal_kembali'] ?></td>
												<td><?php echo $data['nama_pegawai'] ?></td>
												<td><?php echo $data['nama_barang'] ?></td>
												<td><?php echo $data['jumlah_pinjam'] ?></td>
       										</tr>	
											<?php } ?>
                                        </tbody>
                                    </table>
									
                                </div>
                            </div>
<?php include ('footer.php'); ?>