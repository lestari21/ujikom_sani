<?php include("cek.php"); error_reporting(0); ?>

<?php include ('cek_level.php'); ?>

<?php
 
include "koneksi.php";
$query = "SELECT max(kode_jenis) as maxKode FROM jenis";
$hasil = mysqli_query($conn,$query);
$data = mysqli_fetch_array($hasil);
$kodeJenis = $data['maxKode'];
$noUrut = (int) substr($kodeJenis, 4, 4);
$noUrut++;
$char = "JNBG";
$kodeJenis = $char . sprintf("%04s", $noUrut);
?>

<?php include ('header.php'); ?>
							<div class="box box-primary">
                                <div class="box-header">

                                    <i class="fa fa-edit"></i>
									<h3 class="box-title">
                                        Input Data Jenis
                                    </h3>
                                </div>
                                <div class="box-body table-responsive">
									
                                    <form role="form" method="post" action="simpan_jenis.php">
                                    <div class="box-body">
										
										<div class="form-group col-md-4">
											<label>Kode Jenis </label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-barcode"></i>
                                            </div>
                                            <input name="kode_jenis" value="<?php echo $kodeJenis; ?>" type="text" class="form-control" maxlength="10" required readonly />
                                        </div>
										</div>
										
										<div class="form-group col-md-4">
											<label>Nama Jenis </label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-qrcode"></i>
                                            </div>
                                            <input name="nama_jenis" type="text" pattern="[A-Za-z ]+" title="Hanya Boleh Huruf" class="form-control" placeholder="Masukkan Nama Jenis"  maxlength="40" required autofocus />
                                        </div>
										</div>
										
										<div class="form-group col-md-4">
											<label>Keterangan</label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-bars"></i>
                                            </div>
                                            <input name="keterangan_jenis" type="text" pattern="[A-Za-z ]+" title="Hanya Boleh Huruf" class="form-control" placeholder="Masukkan Keterangan" maxlength="25" required autofocus />
                                        </div>
										</div>
										
                                    </div>

                                    <div class="box-body">
                                        <button type="submit" class="btn btn-outline btn-primary">Submit</button>
										<button type="reset" class="btn btn-outline btn-danger">Reset</button>
                                    </div>
                                </form>
									
                                </div>
                            </div>
                            
                            <div class="box box-primary">
                                <div class="box-header">

									<div class="pull-right box-tools">
										<div class="btn-group">
                                           <button class="btn btn-primary fa fa-print" data-toggle="dropdown"></button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li><a href="print_jenis.php">Print to PDF</a></li>
                                                <li><a href="proses_jenis.php">Export to Excel</a></li>
                                            </ul>
                                        </div> &nbsp;
									</div>
								
                                    <i class="fa fa-file-text"></i>
									<h3 class="box-title">
                                        Data Jenis
                                    </h3>
                                </div>
                                <div class="box-body table-responsive">
									
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
											<th> No </th>
											<th> Kode Jenis </th>
											<th> Nama Jenis </th>
											<th> Keterangan </th>
											<th> Aksi </th>
										</thead>
										<tbody>
                                            <?php
												include "koneksi.php";
												$no=1;
												$select=mysqli_query($conn,"SELECT * FROM jenis where status_hapus='1'");
												while($data=mysqli_fetch_array($select))
												{
											?>
                        
											<tr class="success">
												<td><?php echo $no++; ?></td>
												<td><?php echo $data['kode_jenis'] ?></td>
												<td><?php echo $data['nama_jenis'] ?></td>
												<td><?php echo $data['keterangan_jenis'] ?></td>
												<td><a href="#"><button type="button" class="btn btn-outline btn-primary fa fa-edit" data-toggle="modal" data-target="#myModal<?php echo $data['id_jenis']; ?>" title="Edit Data"></button></a>
													<a href="hapus_refresh_jenis.php?id_jenis=<?php echo $data['id_jenis']; ?>"><button type="button" class="btn btn-outline btn-danger fa fa-trash-o" data-toggle="tooltip" title="Hapus Data"
													onClick = "return confirm('Apakah Anda yakin akan menghapus data ini?')"></button></a>
												</td>
       										</tr>
											
											<div class="modal fade" id="myModal<?php echo $data['id_jenis']; ?>" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Data Jenis</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" action="update_jenis.php" method="POST">

                        <?php
                        $id_jenis = $data['id_jenis']; 
                        $query_edit = mysqli_query($conn,"SELECT * FROM jenis WHERE id_jenis='$id_jenis'");
                        //$result = mysqli_query($conn, $query);
                        while ($row = mysqli_fetch_array($query_edit)) {  
                        ?>

                        <input type="hidden" name="id_jenis" value="<?php echo $row['id_jenis']; ?>">

                        <div class="form-group">
                          <label>Kode Jenis</label>
                          <input type="text" name="kode_jenis" class="form-control" value="<?php echo $row['kode_jenis']; ?>" readonly>      
                        </div>
						
						<div class="form-group">
                          <label>Nama Jenis</label>
                          <input type="text" name="nama_jenis" pattern="[A-Za-z ]+" title="Hanya Boleh Huruf" class="form-control" value="<?php echo $row['nama_jenis']; ?>"required />      
                        </div>
						
						<div class="form-group">
                          <label>Keterangan</label>
                          <input type="text" name="keterangan_jenis" pattern="[A-Za-z ]+" title="Hanya Boleh Huruf" class="form-control" value="<?php echo $row['keterangan_jenis']; ?>" required />      
                        </div>
                        
                        <div class="modal-footer">  
                          <button type="submit" class="btn btn-success">Update</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>

                        <?php 
                        }
                        //mysql_close($host);
                        ?>        
                      </form>
                  </div>
                </div>
                
              </div>
            </div>
											
											<?php } ?>
                                        </tbody>
                                    </table>
									
                                </div>
                            </div>
<?php include ('footer.php'); ?>