<?php include("cek.php"); error_reporting(0); ?>

<?php include ('cek_level.php'); ?>
<?php
 
include "koneksi.php";
$query = "SELECT max(kode_ruang) as maxKode FROM ruang";
$hasil = mysqli_query($conn,$query);
$data = mysqli_fetch_array($hasil);
$kodeRuang = $data['maxKode'];
$noUrut = (int) substr($kodeRuang, 4, 4);
$noUrut++;
$char = "RPBG";
$kodeRuang = $char . sprintf("%04s", $noUrut);
?>
<?php include ('header.php'); ?>
							<div class="box box-primary">
                                <div class="box-header">

                                    <i class="fa fa-edit"></i>
									<h3 class="box-title">
                                        Input Data Ruang
                                    </h3>
                                </div>
                                <div class="box-body table-responsive">
									
                                    <form role="form" method="post" action="simpan_ruang.php">
                                    <div class="box-body">
										
										<div class="form-group col-md-4">
											<label>Kode Ruang </label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-ellipsis-h"></i>
                                            </div>
                                            <input name="kode_ruang" value="<?php echo $kodeRuang; ?>" type="text" class="form-control" maxlength="10" required readonly />
                                        </div>
										</div>
										
										<div class="form-group col-md-4">
											<label>Nama Ruang </label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-building-o"></i>
                                            </div>
                                            <input name="nama_ruang" type="text" pattern="[A-Za-z0-9 ]+" title="Hanya Boleh Huruf" class="form-control" placeholder="Masukkan Nama Ruang"  maxlength="40" required autofocus />
                                        </div>
										</div>
										
										<div class="form-group col-md-4">
											<label>Keterangan</label>
                                        <div class="input-group col-md-11">
                                            <div class="input-group-addon">
                                                <i class="fa fa-bars"></i>
                                            </div>
                                            <input name="keterangan_ruang" type="text" pattern="[A-Za-z0-9 ]+" title="Hanya Boleh Huruf" class="form-control" placeholder="Masukkan Keterangan" maxlength="30" required autofocus />
                                        </div>
										</div>
										
                                    </div>

                                    <div class="box-body">
                                        <button type="submit" class="btn btn-outline btn-primary">Submit</button>
										<button type="reset" class="btn btn-outline btn-danger">Reset</button>
                                    </div>
                                </form>
									
                                </div>
                            </div>
							
                            <div class="box box-primary">
                                <div class="box-header">

									<div class="pull-right box-tools">
										<div class="btn-group">
                                           <button class="btn btn-primary fa fa-print" data-toggle="dropdown"></button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li><a href="print_ruang.php">Print to PDF</a></li>
                                                <li><a href="proses_ruang.php">Export to Excel</a></li>
                                            </ul>
                                        </div> &nbsp;
									</div>
								
                                    <i class="fa fa-file-text"></i>
									<h3 class="box-title">
                                        Data Ruang 
                                    </h3>
                                </div>
                                <div class="box-body table-responsive">
									
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
											<th> No </th>
											<th> Kode Ruang </th>
											<th> Nama Ruang </th>
											<th> Keterangan </th>
											<th> Aksi </th>
										</thead>
										<tbody>
                                            <?php
												include "koneksi.php";
												$no=1;
												$select=mysqli_query($conn,"SELECT * FROM ruang where status_hapus='1'");
												while($data=mysqli_fetch_array($select))
												{
											?>
                        
											<tr class="success">
												<td><?php echo $no++; ?></td>
												<td><?php echo $data['kode_ruang'] ?></td>
												<td><?php echo $data['nama_ruang'] ?></td>
												<td><?php echo $data['keterangan_ruang'] ?></td>
												<td><a href="#"><button type="button" class="btn btn-outline btn-primary fa fa-edit" data-toggle="modal" data-target="#myModal<?php echo $data['id_ruang']; ?>" title="Edit Data"></button></a>
													<a href="hapus_refresh_ruang.php?id_ruang=<?php echo $data['id_ruang']; ?>"><button type="button" class="btn btn-outline btn-danger fa fa-trash-o" data-toggle="tooltip" title="Hapus Data"
													onClick = "return confirm('Apakah Anda yakin akan menghapus data ini?')"></button></a>
												</td>
       										</tr>
											
											<div class="modal fade" id="myModal<?php echo $data['id_ruang']; ?>" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Data Ruang</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" action="update_ruang.php" method="POST">

                        <?php
                        $id_ruang = $data['id_ruang']; 
                        $query_edit = mysqli_query($conn,"SELECT * FROM ruang WHERE id_ruang='$id_ruang'");
                        //$result = mysqli_query($conn, $query);
                        while ($row = mysqli_fetch_array($query_edit)) {  
                        ?>

                        <input type="hidden" name="id_ruang" value="<?php echo $row['id_ruang']; ?>">

                        <div class="form-group">
                          <label>Kode Ruang</label>
                          <input type="text" name="kode_ruang" class="form-control" value="<?php echo $row['kode_ruang']; ?>" readonly>      
                        </div>
						
						<div class="form-group">
                          <label>Nama Ruang</label>
                          <input type="text" name="nama_ruang" pattern="[A-Za-z0-9 ]+" title="Hanya Boleh Huruf" class="form-control" value="<?php echo $row['nama_ruang']; ?>" required />      
                        </div>
						
						<div class="form-group">
                          <label>Keterangan</label>
                          <input type="text" name="keterangan_ruang" pattern="[A-Za-z0-9 ]+" title="Hanya Boleh Huruf" class="form-control" value="<?php echo $row['keterangan_ruang']; ?>" required />      
                        </div>
                        
                        <div class="modal-footer">  
                          <button type="submit" class="btn btn-success">Update</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>

                        <?php 
                        }
                        //mysql_close($host);
                        ?>        
                      </form>
                  </div>
                </div>
                
              </div>
            </div>
											<?php } ?>
                                        </tbody>
                                    </table>
									
                                </div>
                            </div>
<?php include ('footer.php'); ?>