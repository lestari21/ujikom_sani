<?php
include("cek.php");
error_reporting(0);
?>
<?php
include ('cek_level.php');
?>
<?php include ('header.php'); ?>
                            <div class="box box-primary">
                                <div class="box-header">
                                    <i class="fa fa-file-text"></i>
									<h3 class="box-title">
                                        Data Laporan
                                    </h3>
                                </div>
                                <div class="box-body">
								<div class="row">
								
									<div class="col-md-2">
									</div>
									
									<div class="col-md-4">
										<div class="box box-danger">
											<div align="center" class="box-body">
											<img src="../img/7.jpg" width="100">
											<h4 align="center"> Laporan </h4>
											<p align="center"> Lihat & Download Data Laporan Transaksi Inventaris. </p>
											</div>
											<div class="box-footer">
												&nbsp;&nbsp; <a href="view.php"><button type="submit" class="btn btn-primary"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; View Data &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </button></a>&nbsp;&nbsp;&nbsp;&nbsp;
												<button type="submit" class="btn btn-primary" data-toggle="dropdown"> &nbsp;&nbsp;&nbsp; Download &nbsp;&nbsp;<span class="fa fa-caret-down"></span> &nbsp;&nbsp;&nbsp;</button>
												<ul class="dropdown-menu pull-right" role="menu">
													<li><a href="print_laporan.php">PDF</a></li>
													<li><a href="proses_laporan.php">Excel</a></li>
												</ul>
											</div> 
										</div>
									</div>
									
									<div class="col-md-4">
										<div class="box box-info">
											<div align="center" class="box-body">
											<img src="../img/4.png" width="100">
											<h4 align="center"> Laporan Perbulan </h4>
											<p align="center"> Download Laporan Inventaris Perbulan.</p>
											</div>
											<div class="box-footer">
													<center><form method="POST" action="laporan.php">
														<select name="bulan" required class="select2_group form-control">
															<option value="">- Pilih Bulan - </option>
															<option value="01">Januari</option>
															<option value="02">Februari</option>
															<option value="03">Maret</option>
															<option value="04">April</option>
															<option value="05">Mei</option>
															<option value="06">Juni</option>
															<option value="07">Juli</option>
															<option value="08">Agustus</option>
															<option value="09">September</option>
															<option value="10">Oktober</option>
															<option value="11">November</option>
															<option value="12">Desember</option>
														</select><br/>
														<input type="submit" value="Pilih" name="pencarian" class="btn btn-primary">
													</form>
												<a href="print_laporan_perbulan.php?bulan=<?php echo $_POST['bulan'] ?>"><button type="submit" class="btn btn-primary btn-block"> &nbsp;&nbsp;&nbsp; Download &nbsp;&nbsp;&nbsp;</button></a>
											</div> 
										</div>
									</div>
									
								</div>
                                </div>
                            </div>
<?php include ('footer.php'); ?>