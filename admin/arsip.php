<?php
include("cek.php");
error_reporting(0);
?>
<?php
include ('cek_level.php');
?>
<?php include ('header.php'); ?>
                            <div class="box box-primary">
                                <div class="box-header">
                                    <i class="fa fa-file-text"></i>
									<h3 class="box-title">
                                        Arsip Data
                                    </h3>
                                </div>
                                <div class="box-body">
								<div class="row">
									
						<div align="center" class="col-lg-6">
                            <!-- small box -->
                            <div class="small-box bg-blue">
                                <div class="inner">
                                    <h4> Arsip Pegawai </h4>
                                    <p> yang tidak aktif / keluar </p>
                                </div>
                                <a href="data_pegawai.php" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
						
						<div align="center" class="col-lg-6">
                           &nbsp; <br/>&nbsp;<br/>&nbsp;<br/>&nbsp;<br/>&nbsp;<br/>&nbsp;<br/><br/>
                        </div>
						
						<div align="center" class="col-lg-4">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h4> Data Jenis Terhapus </h4>
                                    <p> Data Jenis yang sudah dihapus </p>
                                </div>
                                <a href="data_hapus_jenis.php" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
						
						<div align="center" class="col-lg-4">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h4> Data Ruang Terhapus </h4>
                                    <p> Data Ruang yang sudah dihapus </p>
                                </div>
                                <a href="data_hapus_ruang.php" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
						
						<div align="center" class="col-lg-4">
                            <!-- small box -->
                            <div class="small-box bg-maroon">
                                <div class="inner">
                                    <h4> Data Pegawai Terhapus </h4>
                                    <p> Data Pegawai yang sudah dihapus </p>
                                </div>
                                <a href="data_hapus_pegawai.php" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
						
						<div align="center" class="col-lg-4">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h4> Data Petugas Terhapus </h4>
                                    <p> Data Petugas yang sudah dihapus </p>
                                </div>
                                <a href="data_hapus_petugas.php" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
						
						<div align="center" class="col-lg-4">
                            <!-- small box -->
                            <div class="small-box bg-purple">
                                <div class="inner">
                                    <h4> Data Level Terhapus</h4>
                                    <p> Data Level yang sudah dihapus </p>
                                </div>
                                <a href="data_hapus_level.php" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
						
						<div align="center" class="col-lg-4">
                            <!-- small box -->
                            <div class="small-box bg-teal">
                                <div class="inner">
                                    <h4> Data Inventaris Terhapus </h4>
                                    <p> Data Inventaris yang sudah dihapus </p>
                                </div>
                                <a href="data_hapus_inventaris.php" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
						
						
								</div>
                                </div>
                            </div>
<?php include ('footer.php'); ?>