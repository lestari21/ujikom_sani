<?php include("cek.php"); error_reporting(0); ?>

<?php include ('cek_level.php'); ?>
<?php include ('header.php'); ?>  
                            <div class="box box-primary">
                                <div class="box-header">
								
									<div class="pull-right box-tools">
										<div class="btn-group">
                                           <button class="btn btn-primary fa fa-print" data-toggle="dropdown"></button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li><a href="print_arsip_pegawai.php">Print to PDF</a></li>
                                                <li><a href="proses_arsip_pegawai.php">Export to Excel</a></li>
                                            </ul>
                                        </div> &nbsp;
									</div>
								
                                    <i class="fa fa-file-text"></i>
									<h3 class="box-title">
                                        Arsip Data Pegawai
                                    </h3>
                                </div>
								<p>&nbsp; &nbsp; Data pegawai yang sudah tidak aktif/keluar</p>
                                <div class="box-body table-responsive">
									
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
											<th> No </th>
											<th> Nip </th>
											<th> Nama Pegawai </th>
											<th> Jenis Kelamin </th>
											<th> Alamat </th>
											<th> Status </th>
											<th> Aksi </th>
										</thead>
										<tbody>
                                            <?php
												include "koneksi.php";
												$no=1;
												$select=mysqli_query($conn,"SELECT * FROM pegawai where status='Tidak Aktif' AND status_hapus='1' order by nama_pegawai ASC");
												while($data=mysqli_fetch_array($select))
												{
											?>
                        
											<tr class="success">
												<td><?php echo $no++; ?></td>
												<td><?php echo $data['nip'] ?></td>
												<td><?php echo $data['nama_pegawai'] ?></td>
												<td><?php echo $data['jk'] ?></td>
												<td><?php echo $data['alamat'] ?></td>
												<td><?php echo $data['status'] ?></td>
												<td><a href="hapus_pegawai.php?id_pegawai=<?php echo $data['id_pegawai']; ?>"><button type="button" class="btn btn-outline btn-danger fa fa-trash-o" data-toggle="tooltip" title="Hapus Data"
												onClick = "return confirm('Apakah Anda yakin akan menghapus data <?php echo $data['nama_pegawai']; ?> ini? Data akan dihapus secara permanen.')"></button></a>
													<a href="ubah_status_kembali.php?id_pegawai=<?php echo $data['id_pegawai']; ?>"><button type="button" class="btn btn-outline btn-success fa fa-pencil" data-toggle="tooltip" title="Ubah Status"
													onClick = "return confirm('Apakah Anda yakin akan mengubah status data <?php echo $data['nama_pegawai']; ?> ini mejadi aktif?')"></button></a>
												</td>
       										</tr>
											<?php } ?>
                                        </tbody>
                                    </table>
									
                                </div>
                            </div>
<?php include ('footer.php'); ?>