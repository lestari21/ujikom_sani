<?php include("cek.php"); error_reporting(0); ?>

<?php include ('cek_level.php'); ?>

<?php include ('header.php'); ?>                      
                            <div class="box box-primary">
                                <div class="box-header">

                                    <i class="fa fa-edit"></i>
									<h3 class="box-title">
                                        Input Data Pengembalian
                                    </h3>
                                </div>
                                <div class="box-body table-responsive">
									
									<table id="example1" class="table table-bordered table-striped">
                                        <thead>
											<th> No </th>
											<th> Kode Peminjaman </th>
											<th> Nama Pegawai </th>
											<th> Nama Barang </th>
											<th> Aksi </th>
										</thead>
										<tbody>
                                            <?php
												include "koneksi.php";
												$no=1;
												$select=mysqli_query($conn,"SELECT * from peminjaman p left join detail_pinjam d on d.kode_peminjaman=p.kode_peminjaman
														 left join inventaris i on d.id_inventaris=i.id_inventaris
														 left join pegawai g on p.id_pegawai=g.id_pegawai
														 where status_peminjaman='Dipinjam'");
												while($data=mysqli_fetch_array($select))
												{
											?>
                        
											<tr class="success">
												<td><?php echo $no++; ?></td>
												<td><?php echo $data['kode_peminjaman'] ?></td>
												<td><?php echo $data['nama_pegawai'] ?></td>
												<td><?php echo $data['nama_barang'] ?></td>
												<td><a href="#"><button type="button" class="btn btn-outline btn-success fa fa-check" data-toggle="modal" data-target="#myModal<?php echo $data['id_peminjaman']; ?>" title="Kembalikan Barang"></button></a></td>
       										</tr>
											
											<div class="modal fade" id="myModal<?php echo $data['id_peminjaman']; ?>" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Data Pengembalian</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" action="simpan_pengembalian.php" method="POST">

                        <?php
                        $id_peminjaman = $data['id_peminjaman']; 
                        $query_edit = mysqli_query($conn,"select * from peminjaman p left join detail_pinjam d on d.kode_peminjaman=p.kode_peminjaman
														 left join inventaris i on d.id_inventaris=i.id_inventaris
														 left join pegawai g on p.id_pegawai=g.id_pegawai
														 where id_peminjaman='$id_peminjaman'");
                        //$result = mysqli_query($conn, $query);
                        while ($row = mysqli_fetch_array($query_edit)) {  
                        ?>

                        <input type="hidden" name="id_peminjaman" value="<?php echo $row['id_peminjaman']; ?>">

                        <div class="form-group">
                          <label>Kode Peminjaman</label>
                          <input type="text" name="kode_peminjaman" class="form-control" value="<?php echo $row['kode_peminjaman']; ?>" readonly >      
                        </div>
						<div class="form-group">
                          <label>NIP</label>
                          <input type="text" name="nip" class="form-control" value="<?php echo $row['nip']; ?>" readonly>      
                        </div>
                        <div class="form-group">
                          <label>Nama Pegawai</label>
                          <input type="text" name="nama_pegawai" class="form-control" value="<?php echo $row['nama_pegawai']; ?>" readonly>      
                        </div>
						<input type="hidden" name="id_inventaris" value="<?php echo $row['id_inventaris']; ?>">
						<div class="form-group">
                          <label>Nama Barang</label>
                          <input type="text" name="nama_barang" class="form-control" value="<?php echo $row['nama_barang']; ?>" readonly>      
                        </div>
						<div class="form-group">
                          <label>Jumlah Barang</label>
                          <input type="text" name="jumlah_pinjam" class="form-control" value="<?php echo $row['jumlah_pinjam']; ?>" readonly>      
                        </div>
						<div class="form-group">
                          <label>Tanggal Pinjam</label>
                          <input type="text" name="tanggal_pinjam" class="form-control" value="<?php echo $row['tanggal_pinjam']; ?>" readonly>      
                        </div>
						
                        <div class="modal-footer">  
                          <button type="submit" class="btn btn-success">Kembalikan</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        </div>

                        <?php 
                        }
                        //mysql_close($host);
                        ?>        
                      </form>
                  </div>
                </div>
                
              </div>
            </div>
											
											<?php } ?>
                                        </tbody>
                                    </table>
								
								</div>
                            </div>
<?php include ('footer.php'); ?>