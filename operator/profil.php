<?php
include("cek.php");
error_reporting(0);
?>
<?php
include ('cek_level.php');
?>
<html>
    <?php include ('head.php'); ?>
	
    <body class="skin-black">
        
        <header class="header">
            <a class="logo">
                Operator
            </a>
            
            <nav class="navbar navbar-static-top" role="navigation">
				<div><marquee><h4>Inventaris Sarana dan Prasarana di SMKN 1 CIOMAS </h4></marquee></div>
            </nav>
        </header>
		
        <div class="wrapper row-offcanvas row-offcanvas-left">
            
            <aside class="left-side sidebar-offcanvas">
                
                <section class="sidebar">
                    
                    <ul class="sidebar-menu">
                        <li>
                            <a href="dashboard.php">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a>
                                <i class="fa fa-tasks"></i>
                                <span>Peminjaman</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="input_peminjaman.php"><i class="fa fa-angle-double-right"></i> Peminjaman </a></li>
                                <li><a href="peminjaman.php"><i class="fa fa-angle-double-right"></i> Data Peminjaman </a></li>
                            </ul>
                        </li>
						<li class="treeview">
                            <a>
                                <i class="fa fa-tasks"></i>
                                <span>Pengembalian</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="input_pengembalian.php"><i class="fa fa-angle-double-right"></i> Pengembalian </a></li>
                                <li><a href="pengembalian.php"><i class="fa fa-angle-double-right"></i> Data Pengembalian </a></li>
                            </ul>
                        </li>
						<li>
                            <a href="logout.php">
                                <i class="fa fa-sign-out"></i> <span>Logout</span>
                            </a>
                        </li>
                    </ul>
					<div class="sidebar-footer hidden-small">
                        <a href="profil.php" data-toggle="tooltip" data-placement="top" title="Profil">
                            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                        </a>
						<a href="logout.php" data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                </section>
                
            </aside>

            <aside class="right-side">

                <section class="content">

					<div class="row">
                        
                        <section class="col-lg-12">
                            
                            <div class="box box-primary col-md-6">
                                <div class="box-header">
								
                                    <i class="fa fa-file-text"></i>
									<h3 class="box-title">
                                        Profil Anda
                                    </h3>
                                </div>
                                <div class="box-body">
								<div class="container"><br/>
									<table>
									<?php
										include "koneksi.php";
										$select=mysqli_query($conn,"select * from petugas p left join pegawai d on d.nama_pegawai=p.nama_petugas
																							left join level g on p.id_level=g.id_level
																							where username='".$_SESSION['username']."'");
										while($data=mysqli_fetch_array($select)){
									?>
										
									<tr>
										<td rowspan="10"><img src="../img/3.png" width="200"> </td>
										<td rowspan="10"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
										<td>Username</td>
										<td> : </td>
										<td rowspan="10"> &nbsp;&nbsp; </td>
										<td><?php echo $data['username'] ?></td>
									</tr>
									<tr>
										<td>NIP</td>
										<td> : </td>
										<td><?php echo $data['nip'] ?></td>
									</tr>
									<tr>
										<td>Nama </td>
										<td> : </td>
										<td><?php echo $data['nama_pegawai'] ?></td>
									</tr>
									<tr>
										<td>Alamat</td>
										<td> : </td>
										<td><?php echo $data['alamat'] ?></td>
									</tr>
									<tr>
										<td>Level</td>
										<td> : </td>
										<td><?php echo $data['nama_level'] ?></td>
									</tr>
									<tr>
										<td><a href="#"><button type="button" class="btn btn-outline btn-primary" data-toggle="modal" data-target="#myModal<?php echo $data['id_petugas']; ?>">Ganti Password</button></a></td>
       								</tr>
											
											<div class="modal fade" id="myModal<?php echo $data['id_petugas']; ?>" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Ganti Password</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" action="update_password.php" method="POST">

                        <?php
                        $id_petugas = $data['id_petugas']; 
                        $query_edit = mysqli_query($conn,"SELECT * FROM petugas WHERE id_petugas='$id_petugas'");
                        //$result = mysqli_query($conn, $query);
                        while ($row = mysqli_fetch_array($query_edit)) {  
                        ?>

                        <input type="hidden" name="id_petugas" value="<?php echo $row['id_petugas']; ?>">

                        <div class="form-group">
                          <label>Password Lama</label>
                          <input type="password" pattern="[0-9A-Za-z]+" title="Hanya Boleh Huruf & Angka" name="password_sm" class="form-control" required autofocus/>      
                        </div>
						
						<div class="form-group">
                          <label>Password Baru</label>
                          <input type="password" pattern="[0-9A-Za-z]+" title="Hanya Boleh Huruf & Angka" name="password_tmp" class="form-control" required autofocus/>      
                        </div>
						
						<div class="form-group">
                          <label>Ulangi Password</label>
                          <input type="password" pattern="[0-9A-Za-z]+" title="Hanya Boleh Huruf & Angka" name="password_tmp2" class="form-control" required autofocus/ >      
                        </div>
                        
                        <div class="modal-footer">  
                          <button type="submit" class="btn btn-primary">Simpan</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>

                        <?php 
                        }
                        //mysql_close($host);
                        ?>        
                      </form>
                  </div>
                </div>
                
              </div>
            </div>
									
									<?php }?>
									
									</table>
									<br/><br/><br/>
								</div>
                                </div>
                            </div>
                        </section>
                    </div>
				
                </section>
            </aside>
        </div>    

    </body>
</html>