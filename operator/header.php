<?php include("cek.php"); error_reporting(0); ?>
<?php include ('cek_level.php'); ?>

<html>
    <?php include ('head.php'); ?>
	
    <body class="skin-black">
        
        <header class="header">
            <a class="logo">
                Operator
            </a>
            
            <nav class="navbar navbar-static-top" role="navigation">
				<div><marquee><h4>Inventaris Sarana dan Prasarana di SMKN 1 CIOMAS </h4></marquee></div>
            </nav>
        </header>
		
        <div class="wrapper row-offcanvas row-offcanvas-left">
            
            <aside class="left-side sidebar-offcanvas">
                
                <section class="sidebar">
                    
                    <ul class="sidebar-menu">
                        <li>
                            <a href="dashboard.php">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
						<li>
                            <a href="inventaris.php">
                                <i class="fa fa-tags"></i> <span>Inventaris</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a>
                                <i class="fa fa-tasks"></i>
                                <span>Peminjaman</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="input_peminjaman.php"><i class="fa fa-angle-double-right"></i> Peminjaman </a></li>
                                <li><a href="peminjaman.php"><i class="fa fa-angle-double-right"></i> Data Peminjaman </a></li>
                            </ul>
                        </li>
						<li class="treeview">
                            <a>
                                <i class="fa fa-tasks"></i>
                                <span>Pengembalian</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="input_pengembalian.php"><i class="fa fa-angle-double-right"></i> Pengembalian </a></li>
                                <li><a href="pengembalian.php"><i class="fa fa-angle-double-right"></i> Data Pengembalian </a></li>
                            </ul>
                        </li>
                    </ul>
					<div class="sidebar-footer hidden-small">
                        <a href="profil.php" data-toggle="tooltip" data-placement="top" title="Profil">
                            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                        </a>
						<a href="logout.php" data-toggle="tooltip" data-placement="top" title="Logout"
						onClick = "return confirm('Apakah Anda yakin akan keluar?')">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                </section>
                
            </aside>

            <aside class="right-side">
                <section class="content">
					<div class="row">
						<section class="col-lg-12">