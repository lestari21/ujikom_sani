<?php include("cek.php"); error_reporting(0); ?>

<?php include ('cek_level.php'); ?>

<?php include ('header.php'); ?>
                            <div class="box box-primary">
                                <div class="box-header">
								
                                    <i class="fa fa-file-text"></i>
									<h3 class="box-title">
                                        Data Inventaris
                                    </h3>
                                </div>
                                <div class="box-body table-responsive">
									
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
											<th> No </th>
											<th> Kode Inventaris</th>
											<th> Nama Barang </th>
											<th> Kondisi </th>
											<th> Keterangan </th>
											<th> Jumlah </th>
											<th> Tanggal Register </th>
											<th> Aksi </th>
										</thead>
										<tbody>
                                            <?php
												include "koneksi.php";
												$no=1;
												$select=mysqli_query($conn,"SELECT * FROM inventaris where status_hapus_i='1'");
												while($data=mysqli_fetch_array($select))
												{
											?>
                        
											<tr class="success">
												<td><?php echo $no++; ?></td>
												<td><?php echo $data['kode_inventaris'] ?></td>
												<td><?php echo $data['nama_barang'] ?></td>
												<td><?php echo $data['kondisi'] ?></td>
												<td><?php echo $data['keterangan_barang'] ?></td>
												<td><?php echo $data['jumlah'] ?></td>
												<td><?php echo $data['tanggal_register'] ?></td>
												<td><a href="#"><button type="button" class="btn btn-outline btn-success fa fa-eye" data-toggle="modal" data-target="#myModal<?php echo $data['id_inventaris']; ?>" title="Detail Data"></button></a>
												</td>
       										</tr>
											
											<div class="modal fade" id="myModal<?php echo $data['id_inventaris']; ?>" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Detail Data Inventaris</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" method="POST">
						
                        <?php
                        $id_inventaris = $data['id_inventaris']; 
                        $query_detail = mysqli_query($conn,"SELECT * FROM inventaris i left join jenis j on j.id_jenis=i.id_jenis
																					   left join ruang r on r.id_ruang=i.id_ruang
																					   left join petugas p on p.id_petugas=i.id_petugas
																					   WHERE id_inventaris='$id_inventaris'");
                        //$result = mysqli_query($conn, $query);
                        while ($row = mysqli_fetch_array($query_detail)) {  
                        ?>
									
						<div class="form-group">
							Kode Inventaris &nbsp &nbsp
							: &nbsp <?php echo $row['kode_inventaris']; ?>  
						</div>
						
						<div class="form-group">
							Nama Barang &nbsp &nbsp &nbsp;&nbsp
							: &nbsp <?php echo $row['nama_barang']; ?>  
						</div>
						
						<div class="form-group">
							Kondisi &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp;&nbsp;
							: &nbsp <?php echo $row['kondisi']; ?>  
						</div>
						
						<div class="form-group">
							Keterangan &nbsp &nbsp &nbsp &nbsp &nbsp
							: &nbsp <?php echo $row['keterangan_barang']; ?>  
						</div>
						
						<div class="form-group">
							Jumlah &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
							: &nbsp <?php echo $row['jumlah']; ?>  
						</div>
						
						<div class="form-group">
							Tanggal Register &nbsp 
							: &nbsp <?php echo $row['tanggal_register']; ?>  
						</div>
						
						<div class="form-group">
							Kode Jenis &nbsp &nbsp &nbsp &nbsp &nbsp;&nbsp
							: &nbsp <?php echo $row['kode_jenis']; ?>  
						</div>
						
						<div class="form-group">
							Nama Jenis &nbsp &nbsp &nbsp &nbsp &nbsp  
							: &nbsp <?php echo $row['nama_jenis']; ?>  
						</div>
						
						<div class="form-group">
							Kode Ruang &nbsp &nbsp &nbsp &nbsp 
							: &nbsp <?php echo $row['kode_ruang']; ?>  
						</div>
						
						<div class="form-group">
							Nama Ruang &nbsp &nbsp;&nbsp;&nbsp;&nbsp;
							: &nbsp <?php echo $row['nama_ruang']; ?>  
						</div>
						
						<div class="form-group">
							Nama Petugas &nbsp;&nbsp &nbsp;
							: &nbsp <?php echo $row['nama_petugas']; ?>  
						</div>
									
                        <div class="modal-footer">  
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>

                        <?php 
                        }
                        //mysql_close($host);
                        ?>        
                      </form>
                  </div>
                </div>
                
              </div>
            </div>
											
											
											<?php } ?>
                                        </tbody>
                                    </table>
									
                                </div>
                            </div>
<?php include ('footer.php'); ?>