<?php include("cek.php"); error_reporting(0); ?>
<?php include ('cek_level.php'); ?>

<?php 
include "koneksi.php";
$query = "SELECT max(kode_peminjaman) as maxKode FROM peminjaman";
$hasil = mysqli_query($conn,$query);
$data = mysqli_fetch_array($hasil);
$kodePeminjaman = $data['maxKode'];
$noUrut = (int) substr($kodePeminjaman, 4, 4);
$noUrut++;
$char = "PMJM";
$kodePeminjaman = $char . sprintf("%04s", $noUrut);
?>                  
<?php include ('header.php'); ?>

<div class="box box-primary">
    <div class="box-header">
		<i class="fa fa-edit"></i>
			<h3 class="box-title">
                Input Data Peminjaman
            </h3>
    </div>
    <div class="box-body table-responsive">
		<form role="form" method="post" action="simpan_peminjaman.php">
            <div class="box-body">
										
				<div class="form-group">
					<label>Kode Peminjaman</label>
                    <div class="input-group col-md-6">
                        <div class="input-group-addon">
                            <i class="fa fa-barcode"></i>
                        </div>
                        <input name="kode_peminjaman" value="<?php echo $kodePeminjaman; ?>" type="text" class="form-control" maxlength="10" required readonly />
                    </div>
				</div>
										
				<div class="form-group">
					<label>Nama Pegawai </label>
                    <div class="input-group col-md-6">
                        <div class="input-group-addon">
							<i class="fa fa-tags"></i>
                        </div>
                        <select name="id_pegawai" required class="select2_group form-control">
							<option value="">--- Pilih Pegawai ---</option>
								<?php
									include "koneksi.php";
									$select=mysqli_query($conn,"select * from pegawai where status='Aktif' AND status_hapus='1'");
									while($data=mysqli_fetch_array($select)){
								?>
                                <option value="<?php echo $data['id_pegawai'];?>"><?php echo $data['nama_pegawai'];?></option>
								<?php }?>
                        </select>
                    </div>
				</div>
										
				<div class="form-group">
					<label>Inventaris </label>
                    <div class="input-group col-md-6">
                        <div class="input-group-addon">
                            <i class="fa fa-tags"></i>
                        </div>
                        <select name="id_inventaris" required class="select2_group form-control">
							<option value="">--- Pilih Barang Inventaris ---</option>
								<?php
									include "koneksi.php";
									$select=mysqli_query($conn,"select * from inventaris where status_hapus_i='1'");
									while($data=mysqli_fetch_array($select)){
								?>
                                <option value="<?php echo $data['id_inventaris'];?>"><?php echo $data['nama_barang'];?></option>
							<?php }?>
                        </select>
                    </div>
				</div>
										
				<div class="form-group">
					<label>Jumlah Barang</label>
                    <div class="input-group col-md-6">
                        <div class="input-group-addon">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
						<input name="jumlah_pinjam" type="number" pattern="[0-9]+" title="Hanya Boleh Angka" class="form-control" placeholder="Masukkan Jumlah Barang"  maxlength="10" required autofocus />
                    </div>
				</div>
                                        
           </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-outline btn-primary">Submit</button>
				<button type="reset" class="btn btn-outline btn-danger">Reset</button>
            </div>
        </form>
									
    </div>
</div>
							
<?php include ('footer.php'); ?>