<?php
session_start();
include "koneksi.php";
?>
<!DOCTYPE html>
<html>
    <?php include ('head.php'); ?>

    <body class="skin-blue">
	
	<div class="row">
        <nav class="navbar">
			<div class="navbar-right">
				<ul class="nav navbar-nav">
					<li align="left">
						<a href="index.php"><i class="fa fa-reply"></i><b> Back </b>
                        </a>
					</li> 
					<li> <a></a> </li>
					
				</ul>
			</div>
        </nav>
	</div>

	<br/><br/><br/>
        <div align="center" class="container">
            
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="img/img-01.png" alt="IMG">
				</div>

				<form action="cek_login.php" method="post" class="login100-form validate-form">
					<span class="login100-form-title">
						Silahkan Login
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Username Belum Diisi">
						<input class="input100" type="text" name="username" placeholder="Username">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password Belum Diisi">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>
				</form>
			</div>
			
        </div>

		<script src="vendor/tilt/tilt.jquery.min.js"></script>	
		<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
		<script src="js/main.js"></script>	

    </body>
</html>