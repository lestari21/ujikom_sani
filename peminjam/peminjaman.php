<?php
include("cek.php");
error_reporting(0);
?>
<?php
include ('cek_level.php');
?>
<?php

include "koneksi.php";
$query = "SELECT max(kode_peminjaman) as maxKode FROM peminjaman";
$hasil = mysqli_query($conn,$query);
$data = mysqli_fetch_array($hasil);
$kodePeminjaman = $data['maxKode'];
$noUrut = (int) substr($kodePeminjaman, 4, 4);
$noUrut++;
$char = "PMJM";
$kodePeminjaman = $char . sprintf("%04s", $noUrut);
?>
<!DOCTYPE html>
<html>
<?php include ('head.php'); include ('header.php'); ?>

<body>
	<div class="a-grid">
		<div class="container">
			<div class="w3l-about-heading">
				<h2>Peminjaman</h2>
			</div>
			<div class="agileits-services-grids">
			<form role="form" method="post" action="simpan_peminjaman.php">
                                    <div class="box-body">
										
										<div class="form-group">
											<label>Kode Peminjaman</label>
                                        <div class="input-group col-md-6">
                                            <div class="input-group-addon">
                                                <i class="fa fa-barcode"></i>
                                            </div>
                                            <input name="kode_peminjaman" value="<?php echo $kodePeminjaman; ?>" type="text" class="form-control" maxlength="10" required readonly />
                                        </div>
										</div>
										
										<div class="form-group">
											<label>Nama Pegawai </label>
                                        <div class="input-group col-md-6">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
												<select name="id_pegawai" class="form-control" readonly />
													<?php
													include "koneksi.php";
													$select=mysqli_query($conn,"select * from pegawai g left join petugas e on g.nama_pegawai=e.nama_petugas where username='".$_SESSION['username']."'");
													while($data=mysqli_fetch_array($select)){
													?>
                                                        <option value="<?php echo $data['id_pegawai'];?>"><?php echo $data['nama_pegawai'];?></option>
													<?php }?>
                                                </select>
                                        </div>
										</div>
										
										<div class="form-group">
											<label>Inventaris </label>
                                        <div class="input-group col-md-6">
                                            <div class="input-group-addon">
                                                <i class="fa fa-tags"></i>
                                            </div>
                                            <select name="id_inventaris" required class="select2_group form-control">
												<option value="">--- Pilih Barang Inventaris ---</option>
												<?php
												include "koneksi.php";
												$select=mysqli_query($conn,"select * from inventaris where status_hapus_i='1'");
												while($data=mysqli_fetch_array($select)){
												?>
                                                    <option value="<?php echo $data['id_inventaris'];?>"><?php echo $data['nama_barang'];?></option>
												<?php }?>
                                            </select>
                                        </div>
										</div>
										
										<div class="form-group">
											<label>Jumlah Barang</label>
                                        <div class="input-group col-md-6">
                                            <div class="input-group-addon">
                                                <i class="fa fa-shopping-cart"></i>
                                            </div>
                                            <input name="jumlah_pinjam" type="number" pattern="[0-9]+" title="Hanya Boleh Angka" class="form-control" placeholder="Masukkan Jumlah Barang"  maxlength="10" required autofocus />
                                        </div>
										</div>
                                        
                                    </div>

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-outline btn-primary">Submit</button>
										<button type="reset" class="btn btn-outline btn-danger">Reset</button>
                                    </div>
                                </form>
				
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	
</body>	
</html>