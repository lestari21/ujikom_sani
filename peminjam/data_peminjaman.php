<?php
include("cek.php");
error_reporting(0);
?>
<?php
include ('cek_level.php');
?>
<!DOCTYPE html>
<html>
<?php include ('head.php'); include ('header.php'); ?>

<body>
	<div class="a-grid">
		<div class="container">
			<div class="w3l-about-heading">
				<h2>Data Peminjaman</h2>
			</div>
			<div class="agileits-services-grids">
				<a href="peminjaman.php"><button type="submit" class="btn btn-primary fa fa-plus"> Peminjaman </button></a> <br/>&nbsp;
				<table id="example" class="table table-bordered table-striped">
                                        <thead>
											<th> No </th>
											<th> Tanggal Pinjam </th>
											<th> Kode Peminjaman </th>
											<th> Nama Pegawai </th>
											<th> Nama Barang </th>
											<th> Jumlah Pinjam </th>
											<th> Status Peminjaman </th>
										</thead>
										<tbody>
                                            <?php
												include "koneksi.php";
												$no=1;
												$select=mysqli_query($conn,"SELECT * from peminjaman p left join detail_pinjam d on d.kode_peminjaman=p.kode_peminjaman
														 left join inventaris i on d.id_inventaris=i.id_inventaris
														 left join pegawai g on p.id_pegawai=g.id_pegawai
														 left join petugas e on e.nama_petugas=g.nama_pegawai
														 where status_peminjaman='Dipinjam' AND username='".$_SESSION['username']."'");
												while($data=mysqli_fetch_array($select))
												{
											?>
                        
											<tr class="success">
												<td><?php echo $no++; ?></td>
												<td><?php echo $data['tanggal_pinjam'] ?></td>
												<td><?php echo $data['kode_peminjaman'] ?></td>
												<td><?php echo $data['nama_pegawai'] ?></td>
												<td><?php echo $data['nama_barang'] ?></td>
												<td><?php echo $data['jumlah_pinjam'] ?></td>
												<td><?php echo $data['status_peminjaman'] ?></td>
											</tr>
											<?php } ?>
                                        </tbody>
                                    </table>
				<br/><br/><br/><br/><br/><br/><br/>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //a-about -->
</body>	
</html>